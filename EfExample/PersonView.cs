﻿namespace EfExample
{
    public class PersonView
    {
        public int PersonId { get; set; }
        public string TypeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}