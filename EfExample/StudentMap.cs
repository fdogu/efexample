﻿using System.Data.Entity.ModelConfiguration;

namespace EfExample
{
    public class StudentMap : EntityTypeConfiguration<Student>
    {
        public StudentMap()
        {
            HasRequired(s => s.Person)
                .WithOptional(p => p.Student);
            HasKey(s => s.PersonId);
            Property(s => s.CollegeName)
                .HasMaxLength(50).IsRequired();
        }
    }
}
