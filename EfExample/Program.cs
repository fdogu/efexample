﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new PhonebookDbInitializer());

            using (var context = new PhonebookContext())
            {
                context.Database.Log = Console.Write;

                ConcurrencyExample();

                //var sql = @"UpdateCompanies {0}, {1}";
                //var rowsAffected = 
                //    context.Database.ExecuteSqlCommand(                //        sql, DateTime.Now, true);

                //sql = @"SelectCompanies {0}";
                //var companies = context.Database.SqlQuery<CompanyInfo>(sql, DateTime.Today.AddYears(-25));
                //foreach (var companyInfo in companies)
                //{
                //    Console.WriteLine($"{companyInfo.CompanyId} {companyInfo.CompanyName}");
                //}
            }
        }


        private static void ConcurrencyExample()
        {
            var person = new Person
            {
                BirthDate = new DateTime(1970, 1, 2),
                FirstName = "Richard",
                LastName = "Roe",
                HeightInMeters = 1.76M,
                IsActive = true,
                Address = new Address
                {
                    Street = "Dumlupınar Bulvarı No:1",
                    City = "Çankaya",
                    Province = "ANKARA",
                    PostalCode = "06800",
                },
            };

            int personId;
            using (var context = new PhonebookContext())
            {
                context.People.Add(person);
                context.SaveChanges();
                personId = person.Id;
            }

            try
            {
                using (var context1 = new PhonebookContext())
                {
                    context1.People.Find(personId).IsActive = false;
                    person.IsActive = false;

                    using (var context2 = new PhonebookContext())
                    {
                        context2.People.Find(personId).IsActive = false;
                        context2.SaveChanges();
                    }

                    context1.SaveChanges();
                }
                Console.WriteLine("Concurrency hatası alınmalıydı!");
            }
            catch (DbUpdateConcurrencyException e)
            {
                Console.WriteLine($"Beklenen hata: {e}");
            }

            Console.ReadKey();
        }

        private static void ListDepartments()
        {
            using (var context = new PhonebookContext())
            {
                foreach (var department in context.Departments)
                {
                    Console.WriteLine(
                        "Id:{0}, Name:{1}",
                        department.Id,
                        department.Name);
                }
            }
        }

        private static void DeleteContact()
        {
            using (var context = new PhonebookContext())
            {
                var contactId = 2;
                var contact = context.Contacts.Find(contactId);
                if (contact != null)
                {
                    context.Contacts.Remove(contact);
                    context.SaveChanges();
                }
            }
        }

        private static void ListContacts()
        {
            using (var context = new PhonebookContext())
            {
                foreach (var contact in context.Contacts)
                {
                    Console.WriteLine(
                        "Id:{0}, Name:{1}, Number:{2}",
                        contact.Id,
                        contact.Name,
                        contact.Number);
                }
            }
        }

        private static void EnsureDbExists()
        {
            using (var context = new PhonebookContext())
            {
                context.Database.CreateIfNotExists();
            }
        }

        private static void AddContact()
        {
            using (var context = new PhonebookContext())
            {
                var contact = new Contact
                {
                    Name = "Gokhan Aydın",
                    Number = "+90 555 111 222 33"
                };

                context.Contacts.Add(contact);
                context.SaveChanges();
            }
        }

        private class CompanyInfo
        {
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
        }
    }
}
