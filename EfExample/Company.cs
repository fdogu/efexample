﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class Company
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        // public Address Address { get; set; } = new Address();
        public virtual ICollection<Person> Persons { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
