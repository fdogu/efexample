﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class Person
    {
        public int Id { get; set; }
        public byte[] RowVersion { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDate { get; set; }
        public decimal HeightInMeters { get; set; }
        public byte[] Photo { get; set; }
        public byte[] FamilyPicture { get; set; }
        public bool IsActive { get; set; }
        public int NumberOfCars { get; set; }

        public Address Address { get; set; } = new Address();

        public virtual ICollection<Phone> Phones { get; set; } 
            = new HashSet<Phone>();

        public int? PersonTypeId { get; set; }
        public virtual PersonType PersonType { get; set; }

        public virtual ICollection<Company> Companies { get; set; }

        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
            set
            {
                var names = value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                FirstName = names[0];
                LastName = names[1];
            }
        }

        public PersonState PersonState { get; set; } = PersonState.Unknown;
    }
}

