﻿namespace EfExample
{
    public class PersonViewSubstitute
    {
        public int PersonId { get; set; }
        public string TypeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}