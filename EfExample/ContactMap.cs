﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            Property(p => p.Name)
                    .HasMaxLength(30);

            Property(p => p.Number)
                    .HasMaxLength(20)
                    .IsUnicode(false);
        }
    }
}
