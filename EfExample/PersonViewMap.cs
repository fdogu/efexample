﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class PersonViewMap : EntityTypeConfiguration<PersonView>
    {
        public PersonViewMap()
        {
            HasKey(p => p.PersonId);
            ToTable("PersonView");
        }
    }
}
