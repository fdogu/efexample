﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class PhonebookContext : DbContext
    {
        public PhonebookContext() : base("name=phonebookDb")
        {
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<PersonType> PersonTypes { get; set; }
        public DbSet<Company> Companies { get; set; }

        public DbSet<PersonView> PersonViews { get; set; }

        public IQueryable<PersonViewSubstitute> GetPersonViewSubstitute()
        {
            return from person in People
                   join personType in PersonTypes
                   on person.PersonTypeId equals personType.PersonTypeId
                   select new PersonViewSubstitute
                   {
                       PersonId = person.Id,
                       LastName = person.LastName,
                       FirstName = person.FirstName,
                       TypeName = personType.TypeName
                   };
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new StudentMap());

            modelBuilder.Configurations.Add(new AddressMap());

            modelBuilder.Configurations.Add(new PersonViewMap());
        }
    }
}
