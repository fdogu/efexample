﻿using System.Data.Entity.ModelConfiguration;

namespace EfExample
{
    public class PersonTypeMap : EntityTypeConfiguration<PersonType>
    {
        public PersonTypeMap()
        {
            ToTable("TypeOfPerson");

            HasMany(pt => pt.Persons)
                .WithOptional(p => p.PersonType)
                .HasForeignKey(p => p.PersonTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
