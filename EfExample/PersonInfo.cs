﻿using System.Collections.Generic;

namespace EfExample
{
    public class PersonInfo
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PersonType { get; set; }
        public int Id { get; set; }
        public IEnumerable<string> Phones { get; set; }
    }
}