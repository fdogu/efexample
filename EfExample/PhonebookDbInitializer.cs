﻿using System;
using System.Data.Entity;

namespace EfExample
{
    public class PhonebookDbInitializer : DropCreateDatabaseIfModelChanges<PhonebookContext>
    {
        protected override void Seed(PhonebookContext context)
        {
            context.Database.ExecuteSqlCommand("DROP TABLE dbo.PersonView");
            context.Database.ExecuteSqlCommand(
@"CREATE VIEW dbo.PersonView
AS
SELECT p.Id as PersonId, p.FirstName, p.LastName, pt.TypeName
FROM dbo.Person p
INNER JOIN dbo.PersonTypes pt
ON p.PersonTypeId = pt.PersonTypeId");

            context.Database.ExecuteSqlCommand(
@"CREATE PROCEDURE [dbo].[SelectCompanies]
 @dateAdded as DateTime
AS
BEGIN
 SELECT CompanyId, CompanyName
 FROM Companies
 WHERE DateAdded > @dateAdded
END");

            context.Database.ExecuteSqlCommand(
@"CREATE PROCEDURE dbo.UpdateCompanies
 @dateAdded as DateTime,
 @activeFlag as Bit
AS
BEGIN
 UPDATE Companies
 Set DateAdded = @dateAdded,
 IsActive = @activeFlag
END");

            context.Companies.AddRange(new Company[] {
                new Company { CompanyName = "Company 1", DateAdded = DateTime.Today.AddYears(-10) },
                new Company { CompanyName = "Company 2", DateAdded = DateTime.Today.AddYears(-20) },
                new Company { CompanyName = "Company 3", DateAdded = DateTime.Today.AddYears(-30) },
                new Company { CompanyName = "Company 4", DateAdded = DateTime.Today.AddYears(-40) },
            });


        context.Departments.Add(new Department
            {
                Name = "Metu CC"
            });

            var personType = new PersonType {
                TypeName = "Type One"
            };

            var person = new Person
            {
                LastName = "Mellah",
                FirstName = "Hüseyin",
                IsActive = true,
                Address = new Address
                {
                    Street = "Dumlupınar Bulvarı No:1",
                    City = "Çankaya",
                    Province = "ANKARA",
                    PostalCode = "06800",
                },
                PersonState = PersonState.Active,
                PersonType = personType,
                BirthDate = new DateTime(1990, 7, 1)
            };
            person.Phones.Add(new Phone { Number = "+90 312 210 1111" });
            person.Phones.Add(new Phone { Number = "+90 312 210 2222" });
            context.People.Add(person);
        }
    }
}
