﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public enum PersonState
    {
        Unknown,
        Active,
        Inactive,
    }
}
