﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfExample
{
    public class Phone
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public int PersonId { get; set; }
    }
}
